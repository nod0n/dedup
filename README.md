# delete duplicated files in a more controlled manner then fdupes

## idea
### file structure
```
path1
    file1
    file2
path2
    file1
    file3
```

### example
```
$ dedup --delete --main path1 --secondary path2
>    deleting path2/file1
```
`path2/file1` hash been deleted because it was in a secondary path and has also been found in `path1` with the exact same hash. The file name was the same in this example, but it doesn't has to be the same.

### main path(s)
Main paths are scanned for files and every file is hashed. Every hash is added to hash list. File and directories in the main paths will never be modified/deleted!
### secondary path(s)
Similar as the main paths, but one difference: every file found which does also exist in one of the main paths will be deleted (with the `--delete` parameter, to really delete files).

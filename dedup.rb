#!/usr/bin/env ruby
# frozen_string_literal: true

Encoding.default_internal = Encoding::UTF_8
Encoding.default_external = Encoding::UTF_8

require 'find'
require 'userinput'
require 'optparse'
require_relative 'lib/targets'
require_relative 'lib/target'
require_relative 'lib/identical_files'

Dedup = Struct.new :main, :secondary, :delete, :dry_run, keyword_init: true do
  def delete?
    delete
  end

  def dry_run?
    dry_run
  end
end
dedup = Dedup.new main: [], secondary: [], delete: false, dry_run: false

options = ARGV.options do |o|
  o.version = '0.2.1'
  o.on '-mMAIN_PATH', '--main MAIN_PATH', 'main path' do |p|
    dedup[:main] << p
  end
  o.on '-sSECONDARY_PATH', '--secondary SECONDARY_PATH', 'secondary path' do |p|
    dedup[:secondary] << p
  end
  o.on '--[no-]delete',
       'Do really delete the files. Without this option, files will be ' +
         'hashed but not deleted.'
  o.on '-d', '--[no-]dry_run', 'dry run, nothing but listing files'
end
options.parse!(into: dedup)

if dedup.dry_run? && dedup.delete?
  raise ArgumentError,
        "you can't specify --dry-run and --delete!\n#{options.help}"
end

targets = Targets.new(dedup.main, dedup.secondary, dedup.dry_run?)
targets.calculate_checksums

duplicates_to_delete = targets.duplicates_main_to_secondary
duplicates_to_delete.each do |_, d|
  puts "\n\n"
  d.main.each_with_index { |f, i| printf "main [%s]:\t%s\n", i, f }
  puts
  d.secondary.each_with_index { |f, i| printf "secondary [%s]:\t%s\n", i, f }
  puts

  d.secondary.each do |f|
    if dedup.delete?
      printf "deleting:\t%s\n", f
      File.delete(f) if File.exist?(f)
    else
      printf "would delete:\t%s\n", f
    end
  end
  puts
end

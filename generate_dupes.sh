working_dir=/tmp/tmp.I2CI2t4898

if [[ $working_dir =~ ^/tmp/.* ]]; then
  echo "removing  ${BASH_REMATCH[0]}"
  rm -rf "${BASH_REMATCH[0]}"
fi

for d in "$working_dir"/{1,2,3,4}/{a,b,c,d}; do
  mkdir -p "$d"
  dd if=/dev/random of="$d"/random bs=1M count=20
done

cp "${working_dir}/1/a/random" "${working_dir}/4/c/"
cp "${working_dir}/2/d/random" "${working_dir}/3/b/"

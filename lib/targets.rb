# frozen_string_literal: true

# Abstraction of to two target lists (differentiating between main and
# secondary targets), a list of files with identical content and the dry run
# state.
class Targets
  def initialize(main = [], secondary = [], dry_run = false)
    # Files located inside main targets are never deleted.
    @main = main.map { |t| Target.new(t) }

    # Identical files located inside secondary targets are deleted.
    @secondary = secondary.map { |t| Target.new(t) }

    # key: a string containing a checksum
    # value: an instance of the `IdenticalFiles` class
    # TODO: better name?
    @identical_files = {}
    @dry_run = dry_run
  end

  # Publish main and secondary target arrays.
  attr_reader :main, :secondary

  # Checksums are calculated for each file in all main and secondary targets.
  # This can take a long time, depending on the amount of data.
  def calculate_checksums
    { true => @main, false => @secondary }.each do |main, targets|
      type = main ? 'main' : 'secondary'

      targets.each_with_index do |t, i|
        print_target_info type, t.path, (i + 1).to_s, targets.size.to_s

        t.files.each_with_index do |f, j|
          calculate_file_checksum(f, j, t.files.size, main)
        end
      end
    end
  end

  # Get list of checksums of files, for which there is one copy in at least one
  # main target and one copy in at least one secondary target. We don't care
  # about identical files, if they are all in main targets.
  # TODO: What about multiple identical files in only secondary targets?
  # TODO: better name?
  def duplicates_main_to_secondary
    @identical_files.select do |_, d|
      d.main.any? && d.secondary.any?
    end
  end

  private

  def print_target_info(type, path, target_number, total_num_of_targets)
    printf "%<t>s target:\t%<p>s\n", t: type, p: path
    printf "%<t>s target:\t%<n>s out of %<z>s\n",
           t: type, n: target_number, z: total_num_of_targets
    printf "Scanning and hashing files\n\n"
  end

  # file: path to a file
  # index: nth file (n out of total number of files)
  # file_count: total number of files# if `@hashes[hash]` is nil, initialize with an empty instance of `IdenticalFiles`
  # main: is this file located in a main target?
  def calculate_file_checksum(file, index, file_count, main)
    if @dry_run
      printf "would calculate checksum of file: %s\n", file
    else
      printf "calculating ckecksum of file: %s\n", file
      printf "file: %s out of %s\n", (index + 1).to_s, file_count.to_s

      # TODO: Use a gem instead of calling b2sum? Use a faster algorithm?
      hash = IO.popen(['b2sum', file]) { |o| o.read.gsub(/ .*$/, '') }

      @identical_files[hash] ||= IdenticalFiles.new
      @identical_files[hash].add_path(file, main)
    end
  end
end

# frozen_string_literal: true

# This class represents one or more files with identical content.
# TODO: better name?
class IdenticalFiles
  def initialize(main = [], secondary = [])
    # Array of filesystem paths of files located in main targets
    # This files will not be deleted.
    @main = main
    # Array of files
    # This files are useless copies which will be deleted.
    @secondary = secondary
  end

  attr_reader :main, :secondary

  def add_path(path, main)
    raise "Path #{path} already defined!" if (@main + @secondary).include?(path)

    main ? @main << path : @secondary << path
  end
end

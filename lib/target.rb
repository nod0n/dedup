# frozen_string_literal: true

# A target is a filesystem path and all files located inside this path.
class Target
  def initialize(path)
    @path = path # File system path
    @files = []
  end

  attr_reader :path

  # Array of files which are located inside the target (file system path).
  def files
    if @files.empty?
      Find.find(path) { |p| @files << p if FileTest.file?(p) }
    end

    @files
  end
end
